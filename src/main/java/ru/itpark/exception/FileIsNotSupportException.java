package ru.itpark.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.FORBIDDEN)
public class FileIsNotSupportException extends RuntimeException {
    public FileIsNotSupportException() {
    }

    public FileIsNotSupportException(String message) {
        super(message);
    }

    public FileIsNotSupportException(String message, Throwable cause) {
        super(message, cause);
    }

    public FileIsNotSupportException(Throwable cause) {
        super(cause);
    }

    public FileIsNotSupportException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}

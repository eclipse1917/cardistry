package ru.itpark.entity;

import com.fasterxml.jackson.annotation.JsonGetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.validation.constraints.NotEmpty;
import java.util.Date;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode
public class Comment {
    @Id
    @GeneratedValue
    private int id;
    @ManyToOne
    @JsonIgnore
    private Account account;
    @ManyToOne
    @JsonIgnore
    private Note note;
    @ManyToOne
    @JsonIgnore
    private News news;
    @NotEmpty(message = "Comment can't be empty")
    private String commentText;
    private Date createComment;
    @JsonGetter("userName")
    public String userId() {
        return account.getUsername();
    }

}

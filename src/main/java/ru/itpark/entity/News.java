package ru.itpark.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode
public class News {
    @Id
    @GeneratedValue
    private int id;
    @NotEmpty(message = "Title can't be empty")
    @Size(min = 5, max = 50, message = "The title size should be between 5 and 50")
    private String title;
    @Column(columnDefinition = "TEXT")
    @NotEmpty(message = "Content can't be empty")
    @Size(min = 10, max = 500, message = "The content size should be between 10 and 500")
    private String content;
    private int likes;
    @OneToMany(mappedBy = "news", cascade = CascadeType.ALL)
    @JsonIgnore
    private List<Comment> comments = new ArrayList<>();
    private Date createNews;


}

package ru.itpark.entity;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonGetter;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import java.util.Date;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode
public class Picture {
    @GeneratedValue
    @Id
    private int id;
    private Date createDate;
    private String urlPicture;
    @ManyToOne
    @JsonBackReference
    private Account account;
    private int likes;

    @JsonGetter("userName")
    public String userName() {
        return account.getUsername();
    }
}

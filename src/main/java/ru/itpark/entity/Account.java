package ru.itpark.entity;

import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.fasterxml.jackson.annotation.JsonView;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import ru.itpark.jsonView.View;
import javax.persistence.*;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@JsonView(View.ShortUser.class)
public class Account implements UserDetails {
    @Id
    @GeneratedValue
    private int id;
    @Column(nullable = false, unique = true)
    @NotEmpty(message = "User Name can't be empty")
    @NotNull
    private String username;
    @JsonView(View.FullUser.class)
    @Column(nullable = false)
    @NotEmpty(message = "Password can't be empty")
    @Size(min = 5, message = "The size should be between 5")
    private String password;
    @JsonView(View.FullUser.class)
    @ElementCollection(fetch = FetchType.EAGER)
    private Collection<GrantedAuthority> authorities;
    @JsonView(View.FullUser.class)
    private boolean enabled;
    @JsonView(View.FullUser.class)
    private boolean accountNonExpired;
    @JsonView(View.FullUser.class)
    private boolean accountNonLocked;
    @JsonView(View.FullUser.class)
    private boolean credentialsNonExpired = false;
    @NotEmpty(message = "First Name can't be empty")
    private String firstName;
    @NotEmpty(message = "Last Name can't be empty")
    private String lastName;
    private String phone;
    @Email(message = "incorrect email ")
    @Column(unique = true)
    private String email;
    private String photo;
    private Date createAccount;
    @JsonManagedReference
    @OneToMany(mappedBy = "account", cascade = CascadeType.ALL)
    @JsonView(View.FullUser.class)
    private List<Note> notes = new ArrayList<>();
    @OneToMany(mappedBy = "account", cascade = CascadeType.ALL)
    @JsonView(View.FullUser.class)
    private List<Comment> comments = new ArrayList<>();
    @JsonManagedReference
    @OneToMany(mappedBy = "account", cascade = CascadeType.ALL)
    @JsonView(View.FullUser.class)
    private List<Picture> picture = new ArrayList<>();

    public boolean hasAuthority(String authority) {
        return authorities
                .stream()
                .anyMatch(o -> o.getAuthority().equals(authority));
    }
}

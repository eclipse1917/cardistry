package ru.itpark.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.*;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spi.service.contexts.SecurityContext;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import java.util.List;

@Configuration
@EnableSwagger2
public class SwaggerConfig {
    @Bean
    public Docket docket() {
        return new Docket(DocumentationType.SWAGGER_2)
                .select()
                .apis(RequestHandlerSelectors.basePackage("ru.itpark"))
                .paths(PathSelectors.any())
                .build()
                .securityContexts(List.of(securityContext()))
                .securitySchemes(List.of(oauthSecuritySchema()));
    }

    private OAuth oauthSecuritySchema() {
        List<AuthorizationScope> authorizationScopeList = List.of(
                new AuthorizationScope("read", "read"),
                new AuthorizationScope("write", "write"),
                new AuthorizationScope("admin", "admin")
        );

        List<GrantType> grantTypes = List.of(new ResourceOwnerPasswordCredentialsGrant("/oauth/token"));

        return new OAuth("oauth2", authorizationScopeList, grantTypes);
    }

    private SecurityContext securityContext() {
        return SecurityContext.builder().securityReferences(List.of(securityReference())).build();
    }


    private SecurityReference securityReference() {
        return new SecurityReference("oauth2", new AuthorizationScope[]{});
    }
}

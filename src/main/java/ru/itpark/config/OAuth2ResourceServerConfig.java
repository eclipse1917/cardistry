package ru.itpark.config;

import org.springframework.context.annotation.Configuration;

import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableResourceServer;
import org.springframework.security.oauth2.config.annotation.web.configuration.ResourceServerConfigurerAdapter;


@Configuration
@EnableResourceServer
public class OAuth2ResourceServerConfig extends ResourceServerConfigurerAdapter {
    @Override
    public void configure(HttpSecurity http) throws Exception {
        http
                .csrf().disable()
                .cors()
                .and()
                .authorizeRequests()
                .antMatchers(HttpMethod.OPTIONS).permitAll()
                .antMatchers(
                        "*/v2/api-docs",
                        "*/configuration/ui",
                        "*/swagger-resources",
                        "*/configuration/security",
                        "*/swagger-ui.html",
                        "*/webjars/**",
                        "*/swagger-resources/configuration/ui",
                        "*/swagger-ui.html",
                        "*/swagger-resources/configuration/security"
                ).permitAll()
                .and()
                .authorizeRequests()
                .antMatchers("/**/*.png").permitAll()
                .and()
                .authorizeRequests()
                .antMatchers(HttpMethod.POST, "/api/news").hasAuthority("ADMIN")
                .and()
                .authorizeRequests()
                .antMatchers("/**/*.jpg").permitAll()
                .and()
                .authorizeRequests()
                .antMatchers(HttpMethod.POST, "/api/user").permitAll()
                .anyRequest()
                .authenticated()
                .and()
                .httpBasic()
                .and()
                .sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS)
        ;
    }

}

package ru.itpark.exceptionHandler;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import ru.itpark.response.RestErrorResponse;


@RestControllerAdvice(basePackages = "ru.itpark.controller")
public class RestExceptionHandler {
    @ExceptionHandler
    public ResponseEntity<RestErrorResponse> handle(MethodArgumentNotValidException e) {
        HttpStatus status = HttpStatus.BAD_REQUEST;
        RestErrorResponse response = new RestErrorResponse();

        if (e.getBindingResult().hasErrors()) {
            if (e.getBindingResult().hasFieldErrors("password")) {
                response.setCode(status.value());
                response.setMessage(e.getBindingResult().getFieldError("password").getDefaultMessage());
                response.setField("password");

            } else if (e.getBindingResult().hasFieldErrors("username")) {
                response.setCode(status.value());
                response.setMessage(e.getBindingResult().getFieldError("username").getDefaultMessage());
                response.setField("username");
            } else if (e.getBindingResult().hasFieldErrors("firstName")) {
                response.setCode(status.value());
                response.setMessage(e.getBindingResult().getFieldError("firstName").getDefaultMessage());
                response.setField("firstName");
            } else if (e.getBindingResult().hasFieldErrors("lastName")) {
                response.setCode(status.value());
                response.setMessage(e.getBindingResult().getFieldError("lastName").getDefaultMessage());
                response.setField("lastName");
            } else if (e.getBindingResult().hasFieldErrors("email")) {
                response.setCode(status.value());
                response.setMessage(e.getBindingResult().getFieldError("email").getDefaultMessage());
                response.setField("email");
            } else if (e.getBindingResult().hasFieldErrors("commentText")) {
                response.setCode(status.value());
                response.setMessage(e.getBindingResult().getFieldError("commentText").getDefaultMessage());
                response.setField("commentText");
            } else if (e.getBindingResult().hasFieldErrors("content")) {
                response.setCode(status.value());
                response.setMessage(e.getBindingResult().getFieldError("content").getDefaultMessage());
                response.setField("content");
            } else if (e.getBindingResult().hasFieldErrors("title")) {
                response.setCode(status.value());
                response.setMessage(e.getBindingResult().getFieldError("title").getDefaultMessage());
                response.setField("title");
            } else {
                response.setCode(status.value());
                response.setMessage(e.getMessage());
                response.setField("unknown");
            }
        }


        return new ResponseEntity<>(response, status);
    }
}

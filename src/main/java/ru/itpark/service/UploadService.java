package ru.itpark.service;

import org.springframework.web.multipart.MultipartFile;
import ru.itpark.entity.Account;

import java.io.IOException;

public interface UploadService {
    void uploadPhoto(Account account, MultipartFile file) throws IOException;

    void uploadPicture(Account account, MultipartFile file) throws IOException;
}

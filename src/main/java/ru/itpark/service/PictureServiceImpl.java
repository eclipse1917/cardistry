package ru.itpark.service;

import org.springframework.stereotype.Service;
import ru.itpark.entity.Account;
import ru.itpark.entity.Picture;
import ru.itpark.exception.ForbiddenException;
import ru.itpark.exception.PictureNotFoundException;
import ru.itpark.repository.PictureRepository;

import java.util.List;

@Service
public class PictureServiceImpl implements PictureService {
    public PictureServiceImpl(PictureRepository pictureRepository) {
        this.pictureRepository = pictureRepository;
    }

    private final PictureRepository pictureRepository;

    @Override
    public List<Picture> getAllPicture() {
        return pictureRepository.findAllByOrderByLikesDesc();
    }

    @Override
    public List<Picture> getAllForUser(Account account) {
        return pictureRepository.findAllByAccount_IdOrderByCreateDateDesc(account.getId());
    }

    @Override
    public void deletePicture(Account account, int id) {
        Picture picture = pictureRepository.findById(id).orElseThrow(PictureNotFoundException::new);
        if (picture.getAccount().getId() == account.getId()) {
            pictureRepository.deleteById(id);
        } else throw new ForbiddenException();

    }

    @Override
    public void likeById(int id) {
        pictureRepository.likeById(id);
    }

    @Override
    public void dislikeById(int id) {
        pictureRepository.dislikeById(id);

    }
}

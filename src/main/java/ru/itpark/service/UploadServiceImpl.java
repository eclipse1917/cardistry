package ru.itpark.service;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import ru.itpark.entity.Account;
import ru.itpark.entity.Picture;
import ru.itpark.exception.AccountNotFoundException;
import ru.itpark.exception.FileIsNotSupportException;
import ru.itpark.repository.AccountRepository;
import ru.itpark.repository.PictureRepository;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Date;

@Service
public class UploadServiceImpl implements UploadService {
    private final AccountRepository accountRepository;
    private final PictureRepository pictureRepository;
    private final String uploadPath;

    public UploadServiceImpl(
            @Value("${upload.path}") String uploadPath,
            AccountRepository accountRepository,
            PictureRepository pictureRepository) {
        this.uploadPath = uploadPath;
        this.pictureRepository = pictureRepository;
        this.accountRepository = accountRepository;
    }

    @Override
    public void uploadPhoto(Account account, MultipartFile file) throws IOException {
        if (file.getContentType().equals(MediaType.IMAGE_JPEG_VALUE) ||
                file.getContentType().equals(MediaType.IMAGE_PNG_VALUE)) {
            byte[] bytes = file.getBytes();
            Path pathDir = Paths.get(uploadPath + account.getUsername() + "/accountPhoto");
            Files.createDirectories(pathDir);
            String fileName = System.currentTimeMillis() + file.getOriginalFilename();
            Path pathFile = Paths.get(pathDir.toString() + "/" + fileName);
            Files.write(pathFile, bytes);
            Account changeAccount = accountRepository.findById(account.getId()).orElseThrow(
                    AccountNotFoundException::new);
            if (changeAccount.getPhoto() == null) {
                changeAccount.setPhoto(account.getUsername() + "/" + "accountPhoto" + "/" + fileName);
            } else {
                Path oldFile = Paths.get(uploadPath + changeAccount.getPhoto());
                Files.deleteIfExists(oldFile);
                changeAccount.setPhoto(account.getUsername() + "/" + "accountPhoto" + "/" + fileName);
            }

            accountRepository.save(changeAccount);
        } else throw new FileIsNotSupportException("Not Supported");
    }

    @Override
    public void uploadPicture(Account account, MultipartFile file) throws IOException {
        if (file.getContentType().equals(MediaType.IMAGE_JPEG_VALUE) ||
                file.getContentType().equals(MediaType.IMAGE_PNG_VALUE)) {
            byte[] bytes = file.getBytes();
            Path pathDir = Paths.get(uploadPath + account.getUsername() + "/picture");
            Files.createDirectories(pathDir);
            String fileName = System.currentTimeMillis() + file.getOriginalFilename();
            Path pathFile = Paths.get(pathDir.toString() + "/" + fileName);
            Files.write(pathFile, bytes);
            Picture picture = new Picture();
            picture.setAccount(account);
            picture.setCreateDate(new Date());
            picture.setUrlPicture(account.getUsername() + "/" + "picture" + "/" + fileName);
            pictureRepository.save(picture);
        } else throw new FileIsNotSupportException("Not Supported");
    }
}

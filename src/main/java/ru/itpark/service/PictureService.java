package ru.itpark.service;

import ru.itpark.entity.Account;
import ru.itpark.entity.Picture;

import java.util.List;

public interface PictureService {
    List<Picture> getAllPicture();

    List<Picture> getAllForUser(Account account);

    void deletePicture(Account account, int id);

    void likeById(int id);

    void dislikeById(int id);
}

package ru.itpark.service;

import org.springframework.stereotype.Service;
import ru.itpark.entity.Account;
import ru.itpark.entity.Comment;
import ru.itpark.exception.CommentNotFoundException;
import ru.itpark.exception.ForbiddenException;
import ru.itpark.exception.NewsNotFoundException;
import ru.itpark.exception.NoteNotFoundException;
import ru.itpark.repository.CommentRepository;
import ru.itpark.repository.NewsRepository;
import ru.itpark.repository.NoteRepository;

import java.util.Date;
import java.util.List;

@Service
public class CommentServiceImpl implements CommentService {
    private final CommentRepository commentRepository;
    private final NewsRepository newsRepository;
    private final NoteRepository noteRepository;

    public CommentServiceImpl(CommentRepository commentRepository, NewsRepository newsRepository, NoteRepository noteRepository) {
        this.commentRepository = commentRepository;
        this.newsRepository = newsRepository;
        this.noteRepository = noteRepository;
    }

    @Override
    public List<Comment> getAllForNews(int newsId) {
        return commentRepository.findAllByNews_Id(newsId);
    }

    @Override
    public List<Comment> getAllForNote(int noteId) {
        return commentRepository.findAllByNote_Id(noteId);
    }

    @Override
    public void addCommentForNews(Account account, Comment comment, int newsId) {
        comment.setAccount(account);
        comment.setNews(newsRepository.findById(newsId).orElseThrow(NewsNotFoundException::new));
        comment.setCreateComment(new Date());
        commentRepository.save(comment);

    }

    @Override
    public void addCommentForNote(Account account, Comment comment, int noteId) {
        comment.setAccount(account);
        comment.setNote(noteRepository.findById(noteId).orElseThrow(NoteNotFoundException::new));
        comment.setCreateComment(new Date());
        commentRepository.save(comment);
    }

    @Override
    public void deleteComment(Account account, int id) {
        if (commentRepository.findById(id).orElseThrow(CommentNotFoundException::new)
                .getAccount().getId() == account.getId()) {
            commentRepository.deleteById(id);
        } else throw new ForbiddenException();

    }
}

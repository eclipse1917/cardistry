package ru.itpark.service;

import ru.itpark.entity.Account;
import ru.itpark.entity.Comment;

import java.util.List;

public interface CommentService {
    List<Comment> getAllForNews(int newsId);

    List<Comment> getAllForNote(int noteId);

    void addCommentForNews(Account account, Comment comment, int newsId);

    void addCommentForNote(Account account, Comment comment, int noteId);

    void deleteComment(Account account, int id);
}

package ru.itpark.service;

import ru.itpark.entity.Account;
import ru.itpark.entity.Note;

import java.util.List;

public interface NoteService {
    List<Note> getAccountNotes(int id);

    void addNote(Account account, Note note);

    void deleteNote(Account account, int noteId);


    Note getNote(int noteId);

    void likeById(int id);

    void dislikeById(int id);

    void editNote(Account account, Note note, int noteId);

    List<Note> getAllNote();


}

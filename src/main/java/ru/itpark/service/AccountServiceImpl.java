package ru.itpark.service;

import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import ru.itpark.entity.Account;
import ru.itpark.exception.AccountNotFoundException;
import ru.itpark.repository.AccountRepository;

import java.util.Date;
import java.util.List;


@Service
public class AccountServiceImpl implements AccountService, UserDetailsService {

    private final AccountRepository accountRepository;
    private final PasswordEncoder encoder;


    public AccountServiceImpl(AccountRepository accountRepository, PasswordEncoder encoder) {
        this.accountRepository = accountRepository;
        this.encoder = encoder;
    }


    @Override
    public Account getAccountById(int id) {
        return accountRepository.findById(id).orElseThrow(AccountNotFoundException::new);
    }

    @Override
    public void addAccount(Account account) {
        Account addAcc = new Account();
        addAcc.setAccountNonExpired(true);
        addAcc.setAccountNonLocked(true);
        addAcc.setAuthorities(List.of(
                new SimpleGrantedAuthority("VIEW"),
                new SimpleGrantedAuthority("EDIT")
        ));
        addAcc.setCredentialsNonExpired(true);
        addAcc.setEnabled(true);
        addAcc.setPassword(encoder.encode(account.getPassword()));
        addAcc.setCreateAccount(new Date());
        addAcc.setPhone(account.getPhone());
        addAcc.setLastName(account.getLastName());
        addAcc.setFirstName(account.getFirstName());
        addAcc.setEmail(account.getEmail());
        addAcc.setUsername(account.getUsername());
        accountRepository.save(addAcc);
    }

    @Override
    public void deleteAccount(int id) {
        accountRepository.deleteById(id);
    }

    @Override
    public void editAccount(int id, Account editAcc) {
        Account account = accountRepository.findById(id).orElseThrow(AccountNotFoundException::new);
        account.setEmail(editAcc.getEmail());
        account.setFirstName(editAcc.getFirstName());
        account.setLastName(editAcc.getLastName());
        account.setPhone(editAcc.getPhone());
        accountRepository.save(account);
    }

    @Override
    public Account getByUserName(String userName) {
        return accountRepository.findAccountByUsername(userName).orElseThrow(AccountNotFoundException::new);
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        return accountRepository
                .findAccountByUsername(username)
                .orElseThrow(() -> new UsernameNotFoundException(username));
    }
}

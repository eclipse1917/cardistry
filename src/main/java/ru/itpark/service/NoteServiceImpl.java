package ru.itpark.service;

import org.springframework.stereotype.Service;
import ru.itpark.entity.Account;

import ru.itpark.entity.Note;
import ru.itpark.exception.ForbiddenException;
import ru.itpark.exception.NoteNotFoundException;

import ru.itpark.repository.NoteRepository;

import java.util.Date;
import java.util.List;

@Service
public class NoteServiceImpl implements NoteService {

    private final NoteRepository noteRepository;

    public NoteServiceImpl(NoteRepository noteRepository) {
        this.noteRepository = noteRepository;
    }

    @Override
    public List<Note> getAccountNotes(int id) {
        return noteRepository.findAllByAccount_IdOrderByCreateNoteDesc(id);
    }

    @Override
    public void addNote(Account account, Note note) {
        note.setAccount(account);
        note.setCreateNote(new Date());
        noteRepository.save(note);
    }

    @Override
    public void deleteNote(Account account, int noteId) {
        if (getNote(noteId).getAccount().getId() == account.getId()) {
            noteRepository.deleteById(noteId);
        } else throw new ForbiddenException();
    }


    @Override
    public Note getNote(int noteId) {
        return noteRepository.findById(noteId).orElseThrow(NoteNotFoundException::new);
    }

    @Override
    public void likeById(int id) {
        noteRepository.likeById(id);
    }

    @Override
    public void dislikeById(int id) {
        noteRepository.dislikeById(id);
    }

    @Override
    public void editNote(Account account, Note note, int noteId) {
        Note oldNote = noteRepository.findById(noteId).orElseThrow(NoteNotFoundException::new);
        if (oldNote.getAccount().getId() == account.getId()) {
            oldNote.setContent(note.getContent());
            oldNote.setTitle(note.getTitle());
            noteRepository.save(oldNote);
        } else throw new ForbiddenException();
    }

    @Override
    public List<Note> getAllNote() {
        return noteRepository.findAllByOrderByLikesDesc();
    }
}

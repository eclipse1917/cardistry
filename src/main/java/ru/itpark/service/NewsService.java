package ru.itpark.service;


import ru.itpark.entity.News;


import java.util.List;

public interface NewsService {
    List<News> getAll();

    News getNewsById(int id);

    void addNews(News news);

    void deleteNews(int id);


    void likeById(int id);

    void dislikeById(int id);
}

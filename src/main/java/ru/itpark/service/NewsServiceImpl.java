package ru.itpark.service;

import org.springframework.stereotype.Service;


import ru.itpark.entity.News;
import ru.itpark.exception.NewsNotFoundException;
import ru.itpark.repository.NewsRepository;

import java.util.Date;
import java.util.List;

@Service
public class NewsServiceImpl implements NewsService {
    private final NewsRepository newsRepository;

    public NewsServiceImpl(NewsRepository newsRepository) {
        this.newsRepository = newsRepository;
    }

    @Override
    public List<News> getAll() {
        return newsRepository.findAllByOrderByCreateNewsDesc();
    }

    @Override
    public News getNewsById(int id) {
        return newsRepository.findById(id).orElseThrow(NewsNotFoundException::new);
    }

    @Override
    public void addNews(News news) {
        news.setCreateNews(new Date());
        newsRepository.save(news);
    }

    @Override
    public void deleteNews(int id) {
        newsRepository.deleteById(id);
    }

    @Override
    public void likeById(int id) {
        newsRepository.likeById(id);
    }

    @Override
    public void dislikeById(int id) {
        newsRepository.dislikeById(id);
    }


}

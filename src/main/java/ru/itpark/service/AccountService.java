package ru.itpark.service;

import ru.itpark.entity.Account;

public interface AccountService {
    Account getAccountById(int id);

    void addAccount(Account account);

    void deleteAccount(int id);

    void editAccount(int id, Account editAcc);

    Account getByUserName(String userName);
}

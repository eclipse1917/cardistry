package ru.itpark;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.crypto.password.PasswordEncoder;
import ru.itpark.entity.Account;
import ru.itpark.repository.AccountRepository;

import java.util.Date;
import java.util.List;

//
@SpringBootApplication
public class CardistryApplication {

    //    public static void main(String[] args) {
//        SpringApplication.run(CardistryApplication.class, args);
//    }
    public static void main(String[] args) {
        ConfigurableApplicationContext context = SpringApplication.run(CardistryApplication.class, args);

        {
            AccountRepository repository = context.getBean(AccountRepository.class);
            PasswordEncoder encoder = context.getBean(PasswordEncoder.class);
            if (!repository.findAccountByUsername("admin").isPresent()) {
                repository.save(new Account(
                        1,
                        "admin",
                        encoder.encode("password"),
                        List.of(
                                new SimpleGrantedAuthority("VIEW"),
                                new SimpleGrantedAuthority("EDIT"),
                                new SimpleGrantedAuthority("ADMIN")
                        ),
                        true,
                        true,
                        true,
                        true,
                        "Ildar",
                        "Garipov",
                        "8888452155",
                        "Eclipse1917@gmail.com",
                        null, new Date(), null, null, null
                ));
            }
        }

    }
}

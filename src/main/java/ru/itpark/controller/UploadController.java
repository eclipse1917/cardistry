package ru.itpark.controller;

import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import ru.itpark.entity.Account;
import ru.itpark.service.UploadService;

import java.io.IOException;

@RestController
@RequestMapping("/api/upload")
public class UploadController {
    private final UploadService uploadService;

    public UploadController(UploadService uploadService) {
        this.uploadService = uploadService;
    }

    @PostMapping
    public void uploadPhoto(@AuthenticationPrincipal Account account, @RequestParam MultipartFile file) throws IOException {
        if (!file.isEmpty()) {
            uploadService.uploadPhoto(account, file);
        }
    }

    @PostMapping("/picture")
    public void uploadPicture(@AuthenticationPrincipal Account account, @RequestParam MultipartFile file) throws IOException {
        if (!file.isEmpty()) {
            uploadService.uploadPicture(account, file);
        }
    }
}

package ru.itpark.controller;

import com.fasterxml.jackson.annotation.JsonView;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.*;
import ru.itpark.entity.Account;
import ru.itpark.jsonView.View;
import ru.itpark.service.AccountService;

import javax.validation.Valid;


@CrossOrigin
@RestController
@RequestMapping("/api/user")
public class AccountRestController {
    private final AccountService accountService;

    public AccountRestController(AccountService accountService) {
        this.accountService = accountService;
    }


    @GetMapping
    @JsonView(View.ShortUser.class)
    public Account getAccount(@AuthenticationPrincipal Account account) {
        return accountService.getAccountById(account.getId());
    }

    @PostMapping
    public void addAccount(@RequestBody @Valid Account account) {
        accountService.addAccount(account);
    }

    @DeleteMapping
    public void deleteAccount(@AuthenticationPrincipal Account account) {
        accountService.deleteAccount(account.getId());
    }

    @PutMapping
    public void editAccount(@AuthenticationPrincipal Account account, @RequestBody @Valid Account editAcc) {
        accountService.editAccount(account.getId(), editAcc);
    }

    @GetMapping("/{userName}")
    @JsonView(View.ShortUser.class)
    public Account getAccountByUserName(@PathVariable String userName) {
        return accountService.getByUserName(userName);
    }
}





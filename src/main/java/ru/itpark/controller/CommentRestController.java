package ru.itpark.controller;


import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.*;
import ru.itpark.entity.Account;
import ru.itpark.entity.Comment;
import ru.itpark.service.CommentService;

import javax.validation.Valid;
import java.util.List;

@RestController
@CrossOrigin
@RequestMapping("api/comment")
public class CommentRestController {
    private final CommentService commentService;

    public CommentRestController(CommentService commentService) {
        this.commentService = commentService;
    }

    @GetMapping("news/{newsId}")
    public List<Comment> getCommentForNews(@PathVariable int newsId) {
        return commentService.getAllForNews(newsId);
    }

    @GetMapping("note/{noteId}")
    public List<Comment> getCommentForNote(@PathVariable int noteId) {
        return commentService.getAllForNote(noteId);
    }

    @PostMapping("news/{newsId}")
    public void addCommentForNews(@AuthenticationPrincipal Account account,
                                  @RequestBody @Valid Comment comment, @PathVariable int newsId) {
        commentService.addCommentForNews(account, comment, newsId);
    }

    @PostMapping("note/{noteId}")
    public void addCommentForNote(@AuthenticationPrincipal Account account,
                                  @RequestBody @Valid Comment comment, @PathVariable int noteId) {
        commentService.addCommentForNote(account, comment, noteId);
    }

    @DeleteMapping("/{id}")
    public void deleteComment(@AuthenticationPrincipal Account account, @PathVariable int id) {
        commentService.deleteComment(account, id);
    }
}

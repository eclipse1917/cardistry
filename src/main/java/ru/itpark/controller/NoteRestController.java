package ru.itpark.controller;

import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.*;
import ru.itpark.entity.Account;
import ru.itpark.entity.Note;
import ru.itpark.service.NoteService;

import javax.validation.Valid;
import java.util.List;

@RestController
@CrossOrigin
@RequestMapping("/api/notes")
public class NoteRestController {
    private final NoteService noteService;

    public NoteRestController(NoteService noteService) {
        this.noteService = noteService;
    }

    @GetMapping
    public List<Note> getAccountNotes(@AuthenticationPrincipal Account account) {
        return noteService.getAccountNotes(account.getId());
    }

    @GetMapping("/all")
    public List<Note> getAllNote() {
        return noteService.getAllNote();
    }
    @GetMapping("/{noteId}")
    public Note getNote(@PathVariable int noteId) {
        return noteService.getNote(noteId);
    }

    @PostMapping
    public void addNote(@AuthenticationPrincipal Account account, @RequestBody @Valid Note note) {
        noteService.addNote(account, note);
    }

    @PutMapping("/{noteId}")
    public void editNote(@AuthenticationPrincipal Account account, @RequestBody @Valid Note note, @PathVariable int noteId) {
        noteService.editNote(account, note, noteId);
    }

    @DeleteMapping("/{noteId}")
    public void deleteNote(@AuthenticationPrincipal Account account, @PathVariable int noteId) {
        noteService.deleteNote(account, noteId);
    }

    @PostMapping("/{id}/likes")
    public void like(@PathVariable int id) {
        noteService.likeById(id);
    }

    @DeleteMapping("/{id}/likes")
    public void dislike(@PathVariable int id) {
        noteService.dislikeById(id);
    }

}

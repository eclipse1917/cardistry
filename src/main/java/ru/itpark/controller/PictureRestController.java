package ru.itpark.controller;

import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.*;
import ru.itpark.entity.Account;
import ru.itpark.entity.Picture;
import ru.itpark.service.PictureService;

import java.util.List;

@RestController
@CrossOrigin
@RequestMapping("/api/picture")
public class PictureRestController {

    public PictureRestController(PictureService pictureService) {
        this.pictureService = pictureService;
    }

    private final PictureService pictureService;

    @GetMapping
    public List<Picture> getAllPicture() {
        return pictureService.getAllPicture();
    }

    @GetMapping("/user")
    public List<Picture> getAllForUser(@AuthenticationPrincipal Account account) {
        return pictureService.getAllForUser(account);
    }

    @DeleteMapping("/{id}")
    public void deletePicture(@AuthenticationPrincipal Account account, @PathVariable int id) {
        pictureService.deletePicture(account, id);
    }

    @PostMapping("/{id}/likes")
    public void like(@PathVariable int id) {
        pictureService.likeById(id);
    }

    @DeleteMapping("/{id}/likes")
    public void dislike(@PathVariable int id) {
        pictureService.dislikeById(id);
    }
}

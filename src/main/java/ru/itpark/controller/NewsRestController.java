package ru.itpark.controller;


import org.springframework.web.bind.annotation.*;
import ru.itpark.entity.News;
import ru.itpark.service.NewsService;

import javax.validation.Valid;
import java.util.List;

@RestController
@CrossOrigin
@RequestMapping("/api/news")
public class NewsRestController {
    private final NewsService newsService;

    public NewsRestController(NewsService newsService) {
        this.newsService = newsService;
    }

    @GetMapping
    public List<News> getAllNews() {
        return newsService.getAll();
    }

    @GetMapping("/{id}")
    public News getNews(@PathVariable int id) {
        return newsService.getNewsById(id);
    }

    @PostMapping
    public void addNews(@RequestBody @Valid News news) {
        newsService.addNews(news);
    }

    @DeleteMapping("/{id}")
    public void deleteNews(@PathVariable int id) {
        newsService.deleteNews(id);
    }

    @PostMapping("/{id}/likes")
    public void like(@PathVariable int id) {
        newsService.likeById(id);
    }

    @DeleteMapping("/{id}/likes")
    public void dislike(@PathVariable int id) {
        newsService.dislikeById(id);
    }


}

package ru.itpark.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import ru.itpark.entity.News;

import javax.transaction.Transactional;
import java.util.List;

public interface NewsRepository extends JpaRepository<News, Integer> {
    @Modifying
    @Transactional
    @Query("update News n set n.likes = n.likes + 1 where n.id = :id")
    void likeById(@Param("id") int id);

    @Modifying
    @Transactional
    @Query("update News n set n.likes = n.likes - 1 where n.id = :id")
    void dislikeById(@Param("id") int id);


    List<News> findAllByOrderByCreateNewsDesc();
}

package ru.itpark.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import ru.itpark.entity.Note;

import javax.transaction.Transactional;
import java.util.List;

public interface NoteRepository extends JpaRepository<Note, Integer> {

    List<Note> findAllByAccount_Id(int id);

    @Modifying
    @Transactional
    @Query("update Note n set n.likes = n.likes + 1 where n.id = :id")
    void likeById(@Param("id") int id);

    @Modifying
    @Transactional
    @Query("update Note n set n.likes = n.likes - 1 where n.id = :id")
    void dislikeById(@Param("id") int id);

    List<Note> findAllByAccount_IdOrderByCreateNoteDesc(int id);

    List<Note> findAllByOrderByLikesDesc();
}

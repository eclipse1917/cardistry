package ru.itpark.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import ru.itpark.entity.Picture;

import javax.transaction.Transactional;
import java.util.List;

public interface PictureRepository extends JpaRepository<Picture, Integer> {
    List<Picture> findAllByAccount_IdOrderByCreateDateDesc(int id);

    List<Picture> findAllByOrderByLikesDesc();

    @Modifying
    @Transactional
    @Query("update Picture p set p.likes = p.likes + 1 where p.id = :id")
    void likeById(@Param("id") int id);

    @Modifying
    @Transactional
    @Query("update Picture p set p.likes = p.likes - 1 where p.id = :id")
    void dislikeById(@Param("id") int id);
}
